<?php
$skills = select_query_array($cxn, "SELECT * FROM tb_vaardigheid");
$basics = select_query_array($cxn, "SELECT * FROM tb_vaardigheid WHERE vaardigheid_id = alternatief");
$alternatief = select_query_array($cxn, "SELECT vaardigheid_id, alternatief FROM tb_vaardigheid", "vaardigheid_id", "alternatief");
$vaardigheid = select_query_array($cxn, "SELECT vaardigheid_id, naam FROM tb_vaardigheid", "vaardigheid_id", "naam");
$alternatief_2 = select_query_array($cxn, 
	"SELECT * FROM tb_vaardigheid WHERE vaardigheid_id != alternatief AND alternatief NOT IN 
	(SELECT DISTINCT alternatief FROM tb_vaardigheid WHERE alternatief NOT IN 
	(SELECT alternatief FROM tb_vaardigheid WHERE vaardigheid_id = alternatief))", "vaardigheid_id", "naam");
$categorie = select_query_array($cxn, "SELECT categorie_id, categorie FROM tb_categorie", "categorie_id", "categorie");
$type_vaardigheid = select_query_array($cxn, "SELECT type_vaardigheid_id, type_vaardigheid FROM tb_type_vaardigheid", "type_vaardigheid_id", "type_vaardigheid");

if(count($type_vaardigheid)) {
	$basic_types = array();
	foreach($type_vaardigheid as $id => $type) {
		$basic_types[] = select_query_array($cxn, "SELECT vaardigheid_id, naam FROM tb_vaardigheid WHERE type_vaardigheid = '".$id."'", "vaardigheid_id", "naam");
	}
}

$setting = array(
	"vaardigheid" => 0,
	"alternatief" => 50,
	"basis" => 100
);
?>
<div>
	<form id="alt-percentage-form" action="" method="post">
		<dl>
			<dt>
				<label for="perc-vaardigheid">Vaardigheid</label>
				<input type="number" name="perc-vaardigheid" <?php echo "value=\"".$setting["vaardigheid"]."\" "; ?>/>%
			</dt>
			<dd>
			</dd>

			<dt>
				<label for="perc-alternatief">Alternatief</label>
				<input type="number" name="perc-alternatief" <?php echo "value=\"".$setting["alternatief"]."\" "; ?>/>%
			</dt>
			<dd>
			</dd>

			<dt>
				<label for="perc-basis">Basis</label>
				<input type="number" name="perc-basis" <?php echo "value=\"".$setting["basis"]."\" "; ?>/>%
			</dt>
			<dd>
			</dd>
		</dl>
		<a href="javascript:document.getELementById('alt-percentage-form').submit()"></a>
	</form>
</div>

<div class="vaardigheden-list">
	<h4>Vaardigheden</h4>
	<div class="listbox">
		<ul>
<?php
if(count($skills)) {
	foreach($skills as $skill) {
?>
			<li>
				<a href="javascript:document.getElementById('skill')">
					<span>
						<?php echo $skill["naam"]."\n"; ?>
					</span>
					&ensp;
					<span>
<?php
		if($skill["alternatief"] !== $skill["vaardigheid_id"]) {
			echo "[".$vaardigheid[$alternatief[$skill["vaardigheid_id"]]]."]\n";
		}
?>
					</span>
					&ensp;
					<span>
						<?php echo $categorie[$skill["categorie_id"]]."\n"; ?>
					</span>
				</a>
			</li>
<?php
	}
}
?>
		</ul>
	</div>
	<h5>Basisvaardigheden</h5>
	<ul>
<?php
if(count($basics)) {
	foreach($basics as $basic) {
?>
		<li><?php echo $basic["naam"]; ?></li>
<?php
	}
}
?>
	</ul>
</div>

<div>
	<form id="vaardigheden-form" action="" method="post">
		<input type="hidden" name="bewerking" value="Toevoegen" />
		<dl>
			<dt>
				<div>
					<input type="text" name="name-naam-req" placeholder="Vaardigheid" required
					<?php if(isset($_POST["name-naam-req"])) echo "value=\"".$_POST["name-naam-req"]."\""; ?> />
				</div>
			</dt>
			<dd>
<?php
if(isset($error["name-naam-req"])) echo $error["name-naam-req"];
?>
			</dd>

			<dt>
				<div>
					<select name="type_vaardigheid">
						<option disabled selected hidden>- Kies een type vaardigheid -</option>
<?php
		foreach($type_vaardigheid as $id => $type) {
?>
						<option <?php echo "value=\"".$id."\""; ?>><?php echo $type; ?></option>
<?php
		}
?>
					</select>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["type_vaardigheid"])) echo $error["type_vaardigheid"];
?>
			</dd>

			<dt>
				<div>
					<input type="text" list="categorie" name="categorie-req" placeholder="Categorie" required
					<?php if(isset($_POST["categorie-req"])) echo "value=\"".$_POST["categorie-req"]."\""; ?> />
					<datalist id="categorie">
<?php
if(count($categorie)) {
	foreach($categorie as $id => $cat) {
?>
						<option <?php echo "value=\"".$id."\""; ?>><?php echo $cat; ?></option>
<?php
	}
}
?>
					</datalist>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["categorie-req"])) echo $error["categorie-req"];
?>
			</dd>

			<dt>
				<div>
					<label for="alternatief">Alternatieve vaardigheid</label>
					<select name="alternatief">
						<option value="-1">Geen (Basisvaardigheid)</option>
<?php
if(count($alternatief_2)) {
	foreach($alternatief_2 as $id => $alt) {
?>
						<option <?php echo "value=\"".$id."\"";
		if(isset($_POST["alternatief"]) && $_POST["alternatief"] === $id) {
			echo " selected";
		} ?>>
							<?php echo $alt."\n"; ?>
						</option>
<?php
	}
}
?>
					</select>
<?php
?>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["alternatief"])) echo $error["alternatief"];
?>
			</dd>

		</dl>
		<a class="submit button" href="javascript:document.getELementById('vaardigheden-form').submit()">Toevoegen</a>
	</form>
</div>
