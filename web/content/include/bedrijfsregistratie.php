<?php
if(isset($error)) {
	$nError = count($error);
}

if(isset($post) && !isset($nError)) {
	if(!isset($_SESSION["bedrijfsregistratie"]) && isset($post["name-bedrijfsnaam-req"])) {
		$bedrijf = mysqli_query($cxn, "SELECT naam, adres, postcode, plaats FROM tb_bedrijf WHERE naam = '".$post["name-bedrijfsnaam-req"]."'");
		if(
			$bedrijf !== false && 
			strlower($bedrijf["naam"]) == strtolower($post["name-bedrijfsnaam-req"]) &&
//			strlower($bedrijf["adres"]) == strtolower($post["straat-req"]) &&
			strlower($bedrijf["home_number-huisnummer-req"]) == strtolower($post["home_number-huisnummer-req"]) &&
			strlower($bedrijf["postcode"]) == strtolower($post["postal_code-postcode-req"])// &&
//			strlower($bedrijf["plaats"]) == strtolower($post["plaats-req"])
		) {
			$error["name-bedrijfsnaam-req"] = "Uw bedrijf is bij ons al geregistreerd. Log in en pas uw gegevens aan.";
			$nError = count($error);
		}
	}

	if(!isset($error)) {

		if(isset($_SESSION["bedrijfsregistratie"], $_SESSION["bedrijfsregistratie"]["name-bedrijfsnaam-req"])) {
			if(isset($post["username-req"], $post["password_user-1-wachtwoord-req"], $post["password_user-2-wachtwoord-req"])) {
				if(user_exists($cxn, $post["username-req"])) {
					$error["username-req"] = "Ongeldige gebruikersnaam";
					if($post["password_user-1-wachtwoord-req"] !== $post["password_user-2-wachtwoord-req"]) {
						$error["password-2-wachtwoord-req"] = "Beide wachtwoorden zijn niet hetzelfde.";
					}
				} else {
					if($post["password_user-1-wachtwoord-req"] === $post["password_user-2-wachtwoord-req"]) {
						mysqli_query($cxn, "INSERT INTO tb_users (username, password, emailadres, groep_id) VALUES (".
							"'".$post["username-req"]."', ".
							"'".hash("sha256", $post["password_user-1-wachtwoord-req"].$post["username-req"])."', ".
							"'".$_SESSION["bedrijfsregistratie"]["email-req"]."', ".
							get_id($cxn, "naam", "Bedrijf", "tb_groep").
						")");

						mysqli_query($cxn, "INSERT INTO tb_bedrijf (users_id, naam, straat, huisnummer, postcode, plaats, land, telefoon, emailadres) VALUES (".
							get_id($cxn, "username", $post["username-req"], "tb_users").", ".
							"'".$_SESSION["bedrijfsregistratie"]["name-bedrijfsnaam-req"]."', ".
							"'".$_SESSION["bedrijfsregistratie"]["straat-req"]."', ".
							"'".$_SESSION["bedrijfsregistratie"]["home_number-huisnummer-req"]."', ".
							"'".postal_code($_SESSION["bedrijfsregistratie"]["postal_code-postcode-req"])."', ".
							"'".$_SESSION["bedrijfsregistratie"]["plaats-req"]."', ".
							"'".$_SESSION["bedrijfsregistratie"]["name-land-req"]."', ".
							"'".$_SESSION["bedrijfsregistratie"]["tel_number-telefoon-req"]."', ".
							"'".$_SESSION["bedrijfsregistratie"]["email-req"]."'".
						")");

						if(!(empty($_SESSION["bedrijfsregistratie"]["name-voornaam"]) &&
							empty($_SESSION["bedrijfsregistratie"]["name-voorletters"]) &&
							empty($_SESSION["bedrijfsregistratie"]["tussenvoegsel"]) &&
							empty($_SESSION["bedrijfsregistratie"]["name-achternaam"]) &&
							empty($_SESSION["bedrijfsregistratie"]["functie-contact"]) &&
							empty($_SESSION["bedrijfsregistratie"]["tel_number-contact"]) &&
							empty($_SESSION["bedrijfsregistratie"]["email-contact"])
						)) {
							mysqli_query($cxn, "INSERT INTO tb_contactpersoon (bedrijf_id, voornaam, voorletters, tussenvoegsel, achternaam, functie, telefoon, emailadres) VALUES (".
								get_id($cxn, "naam", $_SESSION["bedrijfsregistratie"]["name-bedrijfsnaam-req"], "tb_bedrijf").", ".
								(empty($_SESSION["bedrijfsregistratie"]["name-voornaam"])    	? null : "'".$_SESSION["bedrijfsregistratie"]["name-voornaam"]."'").", ".
								(empty($_SESSION["bedrijfsregistratie"]["name-voorletters"]) 	? null : "'".$_SESSION["bedrijfsregistratie"]["name-voorletters"]."'").", ".
								(empty($_SESSION["bedrijfsregistratie"]["tussenvoegsel"])    	? null : "'".$_SESSION["bedrijfsregistratie"]["tussenvoegsel"]."'").", ".
								(empty($_SESSION["bedrijfsregistratie"]["name-achternaam"])  	? null : "'".$_SESSION["bedrijfsregistratie"]["name-achternaam"]."'").", ".
								(empty($_SESSION["bedrijfsregistratie"]["functie-contact"])  	? null : "'".$_SESSION["bedrijfsregistratie"]["functie-contact"]."'").", ".
								(empty($_SESSION["bedrijfsregistratie"]["tel_number-contact"])  ? null : "'".$_SESSION["bedrijfsregistratie"]["number-contact"]."'").", ".
								(empty($_SESSION["bedrijfsregistratie"]["email-contact"])    	? null : "'".$_SESSION["bedrijfsregistratie"]["email-contact"]."'").
							")");
						}
						unset($_SESSION["bedrijfsregistratie"]);
					} else {
						$error["password-2-wachtwoord-req"] = "Beide wachtwoorden zijn niet hetzelfde.";
					}
				}

				if(isset($error)) {
					$nError = count($error);
				}
			}
		} else {
			$_SESSION["bedrijfsregistratie"] = $post;
		}
	}
} else {
	if(isset($_GET["terug"])) {
		unset($_SESSION["bedrijfsregistratie"], $_POST, $post);
		header("redirect: ?pagina=bedrijfsregistratie");
		exit;
	}
}

$cities = select_query_array($cxn, "SELECT DISTINCT plaats FROM tb_cursist UNION (SELECT DISTINCT plaats FROM tb_bedrijf WHERE plaats NOT IN (SELECT DISTINCT plaats FROM tb_cursist))");
$countries = select_query_array($cxn, "SELECT DISTINCT land FROM tb_bedrijf", "land");
?>
<h3>Registratie</h3>
<h4>Bedrijf (werkgever)</h4>
<?php
if(isset($_SESSION["bedrijfsregistratie"], $_SESSION["bedrijfsregistratie"]["name-bedrijfsnaam-req"]) && !isset($_GET["terug"])) {
//	echo "SESSION: "; print_r($_SESSION["bedrijfsregistratie"]);

	include "content/include/gebruikersregistratieform.php";
} else {
	if(isset($_GET["terug"], $_SESSION["bedrijfsregistratie"])) {
		$_POST = $_SESSION["bedrijfsregistratie"];
//		unset($_SESSION["bedrijfsregistratie"]);
	}
//	echo "POST: "; print_r($_POST);

	include "content/include/bedrijfsregistratieform.php";
}
?>
