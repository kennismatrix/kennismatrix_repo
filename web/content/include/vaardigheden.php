<?php
$vaardigheid = select_query_array($cxn, "SELECT vaardigheid_id, naam FROM tb_vaardigheid", "vaardigheid_id", "naam");
/*
	TUSSENTABEL tb_cursist_vaardigheid BEVAT NIVEAU
	TUSSENTABEL tb_vacature_vaardigheid BEVAT NIVEAU
*/
if($n = count($vaardigheid)) {
	if($n > 5) {
		$n = 5;
	}
?>
		<h4>Vaardigheden</h4>
		<p>
			Belangrijkste vaardigheden (maximaal <?php echo $n; ?>)
		</p>
		<input type="hidden" id="active-n-skills" name="active-n-skills" value="1" />
		<dl>
<?php
	for($i = 0; $i < $n; $i++) {
?>
			<dt id="vaardigheden" <?php if($i > 0) echo "style=\"display: none\""; ?>>
				<div>
					<select <?php echo "name=\"vaardigheid-".$i."\""; ?>>
<?php
		foreach($vaardigheid as $id => $naam) {
?>
						<option <?php
			echo "value=\"".$id."\"";
			if(isset($_POST["vaardigheid-".$i]) && $_POST["vaardigheid-".$i] === $id) {
				echo " selected";
			} ?>>
							<?php echo $naam."\n"; ?>
						</option>
<?php
		}
?>
					</select>
					<input type="number" <?php echo "name=\"percentage-".$i."-req\""; ?> step="0.01" min="0" max="100" value="0.00" required />%
					<a class="remove button" href="javascript:$('#active-n-skills').val(-=); $('#vaardigheid-' + $('#active-n-skills').val()).show();"">Verwijderen</a>
				</div>
			</dt>
			<dd>
			</dd>
<?php
	}
?>
		</dl>
		<a class="add button" href="javascript:$('#active-n-skills').val(+=); $('#vaardigheid-' + $('#active-n-skills').val()).show();">Toevoegen</a>
<?php
}
?>

