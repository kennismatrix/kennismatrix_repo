<?php
$competenties = select_query_array($cxn, "SELECT * FROM tb_competentie_omschrijving");
$type_competenties = select_query_array($cxn, "SELECT * FROM tb_type_competentie");

$niveaus = select_query_array($cxn, "SELECT niveau_id, kwalificatie FROM tb_niveau", "niveau_id", "kwalificatie");

include "vaardigheden.php";

?>
		<h4>Competenties</h4>
		<dl class="formgroup competenties">
			<dt>
				<div class="check listbox">
					<ul>
<?php
foreach($competenties as $competentie) {
?>
						<li>
							<input type="checkbox" <?php
	echo "id=\"competentie-".$competentie["competentie_omschrijving_id"]."\" ";
	echo "name=\"taken[".$competentie["competentie_omschrijving_id"]."]\"";
	if(isset($_POST["taken"][$competentie["competentie_omschrijving_id"]])) {
		echo " checked";
	}
?> />
							<label <?php echo "for=\"competentie-".$competentie["competentie_omschrijving_id"]."\" onclick=\"javascript:document.getElementById('competentie-".$competentie["competentie_omschrijving_id"]."').checked = document.getElementById('competentie-".$competentie["competentie_omschrijving_id"]."').checked\""; ?>>
								<span>
									<?php echo $competentie["competentie"]."\n"; ?>
								</span>
								&ndash;
								<span>
									<?php echo $niveaus[$competentie["niveau_id"]]."\n"; ?>
								</span>
							</label>
						</li>
<?php
}
?>
					</ul>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["competentie"])) echo $error["competentie"];
?>
			</dd>

			<dt>
				<div>
					<input type="text" name="nieuw_competentie-req" placeholder="Competentie" title="Competentie" required
					<?php if(isset($_POST["nieuw_competentie-req"])) echo "value=\"".$_POST["nieuw_competentie-req"]."\""; ?> />
				</div>
			</dt>
			<dd>
<?php
if(isset($error["nieuw_competentie-req"])) echo $error["nieuw_competentie-req"];
?>
			</dd>

			<dt>
				<div>
					<input type="text" list="nieuw_type" name="nieuw_type-req" placeholder="Type" title="Type competentie" required
					<?php if(isset($_POST["nieuw_type-req"])) echo "value=\"".$_POST["nieuw_type-req"]."\""; ?>
					/>
					<datalist id="nieuw_type">
<?php
if(count($type_competenties)) {
	foreach($type_competenties as $type) {
?>
						<option <?php echo "value=\"".$type["type_competentie"]."\""; ?>></option>
<?php
	}
}
?>
					</datalist>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["nieuw_omschrijving-req"])) echo $error["nieuw_omschrijving-req"];
?>
			</dd>

			<dt>
				<div>
					<textarea name="nieuw_omschrijving-req" placeholder="Omschrijving" title="Omschrijving" required><?php 
						if(isset($_POST["nieuw_omschrijving-req"])) echo "value=\"".$_POST["nieuw_omschrijving-req"]."\"";
					?></textarea>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["nieuw_omschrijving-req"])) echo $error["nieuw_omschrijving-req"];
?>
			</dd>

			<dt>
				<div>
					<select name="nieuw_niveau" title="Niveau">
<?php
if(count($niveaus)) {
	foreach($niveaus as $id => $niveau) {
?>
						<option <?php
		echo "value=\"".$id."\"";
		if(isset($_POST["nieuw_niveau"]) && $_POST["nieuw_niveau"] === $niveau) {
			echo " selected";
		} ?>><?php echo $niveau; ?></option>
<?php
	}
}
?>
					</select>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["nieuw_niveau"])) echo $error["nieuw_niveau"];
?>
			</dd>
		</dl>
