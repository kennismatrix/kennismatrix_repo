<form id="bedrijven-registratie-form" action="?pagina=bedrijfsregistratie" method="post">
	<dl>
		<dt>
			<div>
				<input type="text" name="name-bedrijfsnaam-req" placeholder="Bedrijfsnaam" required
<?php
if(isset($_POST["name-bedrijfsnaam-req"])) {
	echo "value=\"".$_POST["name-bedrijfsnaam-req"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["name-bedrijfsnaam-req"])) echo $error["name-bedrijfsnaam-req"];
?>
		</dd>

		<dt>
			<div>
				<input type="text" name="straat-req" placeholder="Straat" required
<?php
if(isset($_POST["straat-req"])) {
	echo "value=\"".$_POST["straat-req"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["straat-req"])) echo $error["straat-req"];
?>
		</dd>

		<dt>
			<div>
				<input type="text" name="home_number-huisnummer-req" placeholder="Huisnummer" required
<?php
if(isset($_POST["home_number-huisnummer-req"])) {
	echo "value=\"".$_POST["home_number-huisnummer-req"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["home_number-huisnummer-req"])) echo $error["home_number-huisnummer-req"];
?>
		</dd>

		<dt>
			<div>
				<input type="text" name="postal_code-postcode-req" placeholder="Postcode" required
<?php if(isset($_POST["postal_code-postcode-req"])) {
	echo "value=\"".$_POST["postal_code-postcode-req"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["postal_code-postcode-req"])) echo $error["postal_code-postcode-req"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" list="plaats" name="plaats-req" placeholder="Plaats" required
<?php
if(isset($_POST["plaats-req"])) {
	echo "value=\"".$_POST["plaats-req"]."\"";
}
?> />
<?php
if(count($cities)) {
?>
				<datalist id="plaats">
<?php
	foreach($cities as $city) {
?>
					<option <?php echo "value=\"".$city["plaats"]."\""; ?>></option>
<?php
	}
?>
				</datalist>
<?php
}
?>
			</div>
		</dt>
		<dd>
<?php
if(isset($error["plaats"])) echo $error["plaats"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" list="land" name="name-land-req" placeholder="Land" required
<?php
if(isset($_POST["name-land-req"])) {
	echo "value=\"".$_POST["name-land-req"]."\"";
}
?> />
<?php
if(count($countries)) {
?>
				<datalist id="land">
<?php
	foreach($countries as $country) {
?>
					<option <?php echo "value=\"".$country["land"]."\""; ?>></option>
<?php
	}
?>
				</datalist>
<?php
}
?>
			</div>
		</dt>
		<dd>
<?php
if(isset($error["name-land-req"])) echo $error["name-land-req"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="email-req" placeholder="E-mailadres" required
<?php
if(isset($_POST["email-req"])) {
	echo "value=\"".$_POST["email-req"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["email-req"])) echo $error["email-req"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="tel_number-telefoon-req" placeholder="Telefoon" required
<?php
if(isset($_POST["tel_number-telefoon-req"]))
{
	echo "value=\"".$_POST["tel_number-telefoon-req"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["tel_number-telefoon-req"])) echo $error["tel_number-telefoon-req"];
?>
		</dd>
	</dl>
	<h4>Contactpersoon</h4>
	<dl class="formgroup contactpersoon">
		<dt>
			<div>
				<input type="text" name="name-voornaam" placeholder="Voornaam"
<?php
if(isset($_POST["name-voornaam"])) {
	echo "value=\"".$_POST["name-voornaam"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["name-voornaam"])) echo $error["name-voornaam"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="name-voorletters" placeholder="Voorletters"
<?php
if(isset($_POST["name-voorletters"])) {
	echo "value=\"".$_POST["name-voorletters"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["name-voorletters"])) echo $error["name-voorletters"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="tussenvoegsel" placeholder="Tussenvoegsel"
<?php
if(isset($_POST["tussenvoegsel"])) {
	echo "value=\"".$_POST["tussenvoegsel"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["tussenvoegsel"])) echo $error["tussenvoegsel"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="name-achternaam" placeholder="Achternaam"
<?php
if(isset($_POST["name-achternaam"])) {
	echo "value=\"".$_POST["name-achternaam"]."\""; 
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["name-achternaam"])) echo $error["name-achternaam"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="functie-contact" placeholder="Functie"
<?php
if(isset($_POST["functie-contact"])) {
	echo "value=\"".$_POST["functie-contact"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["functie-contact"])) echo $error["functie-contact"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="email-contact" placeholder="E-mailadres contactpersoon"
<?php
if(isset($_POST["email-contact"])) {
	echo "value=\"".$_POST["email-contact"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["email-contact"])) echo $error["email-contact"];
?>
		</dd>
		<dt>
			<div>
				<input type="text" name="tel_number-contact" placeholder="Telefoonnummer contactpersoon"
<?php
if(isset($_POST["tel_number-contact"])) {
	echo "value=\"".$_POST["tel_number-contact"]."\"";
}
?> />
			</div>
		</dt>
		<dd>
<?php
if(isset($error["tel_number-contact"])) echo $error["tel_number-contact"];
?>
		</dd>
	</dl>
	<a href="javascript:document.getElementById('bedrijven-registratie-form').submit()">Kies een gebruikersnaam</a>
</form>







