<?php
$dienstvormen = select_query_array($cxn, "SELECT DISTINCT dienstvorm FROM tb_beschikbaarheid");
?>
		<h4>Beschikbaarheid</h4>
		<dl class="formgroup beschikbaarheid">
			<dt>
				<div>
					<input type="number" name="number-uren-req" placeholder="Uren" title="Uren" required
					<?php if(isset($_POST["number-uren-req"])) echo "value=\"".$_POST["number-uren-req"]."\""; ?> />
					<span>uur per week</span>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["number-uren-req"])) echo $error["number-uren-req"];
?>
			</dd>

			<dt>
				<div>
					<input type="text" list="dienstvorm" name="dienstvorm-req" placeholder="Dienstvorm" title="Dienstvorm" required
					<?php if(isset($_POST["dienstvorm-req"])) echo "value=\"".$_POST["dienstvorm-req"]."\""; ?> />
					<datalist id="dienstvorm">
<?php
if(count($dienstvormen)) {
	foreach($dienstvormen as $dienstvorm) {
?>
						<option <?php echo "value=\"".$dienstvorm["dienstvorm"]."\""; ?>></option>
<?php
	}
}
?>
					</datalist>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["dienstvorm-req"])) echo $error["dienstvorm-req"];
?>
			</dd>

			<dt>
				<h5>Werkdagen</h5>
				<div>
					<ul class="check listbox">
<?php
$working_days = $werkdagen;
$working_days[] = array_shift($working_days);
//print_r($working_days);
foreach($working_days as $werkdag) {
?>
						<li>
							<input type="checkbox" <?php
								echo "id=\"".$werkdag."\" name=\"werkdagen[".$werkdag."]\"";
								if(isset($_POST["werkdagen"][$werkdag])) {
									echo " checked";
								}
							?> />
							<label <?php echo "for=\"werkdagen[".$werkdag."]\" onclick=\"javascript:document.getElementById('".$werkdag."').checked = !document.getElementById('".$werkdag."').checked\""; ?>><?php echo $werkdag; ?></label>
						</li>
<?php
}
?>
					</ul>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["werkdagen"])) echo $error["werkdagen"];
?>
			</dd>

			<dt>
				<div>
					<input type="number" name="number-actieradius-req" placeholder="Actieradius" title="Actieradius" required
					<?php if(isset($_POST["number-actieradius-req"])) echo "value=\"".$_POST["number-actieradius-req"]."\""; ?> />
					<span>km</span>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["number-actieradius-req"])) echo $error["number-actieradius-req"];
?>
			</dd>
		</dl>

