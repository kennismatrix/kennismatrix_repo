<?php
if($_POST && !isset($error) && isset($save2file) && count($save2file)) {
//	print_r($save2file);

	$path = "content/".$content_file_types_post[$page]."/";
	$vacatures = map_files_filter($path, DIR_ONLY);
	if(count($vacatures)) {
		$last_dir = end($vacatures);
		strinc($last_dir);
	} else {
		$last_dir = "100000";
	}

	$path .= ($last_dir."/");

	/* Maak een vacaturemap aan */
	if(mkdir($path)) {

		/* Schrijf tekst weg in bestanden en onthoudt de bestandsnamen(/locatie) */
		$files2save = array();
		foreach($save2file as $field => $value) {
			file_put_contents(($files2save[$field] = ($path.$field.".html")), htmlspecialchars_decode($value));
		}

		/* Voeg functie toe aan de Kennismatrix */
		$functie_id = array_search(mb_convert_case($post["functie-req"], MB_CASE_LOWER, "UTF-8"), array_map("strtolower", select_query_array($cxn, "SELECT functie_id, functie FROM tb_functie", "functie_id", "functie")));
		if($functie_id === false) {
			mysqli_query($cxn, "INSERT INTO tb_functie (functie) VALUES ('".$post["functie-req"]."')");
			$functie_id = array_search($post["functie-req"], select_query_array($cxn, "SELECT functie_id, functie FROM tb_functie", "functie_id", "functie"));
		}

		/* Voeg beschikbaarheid toe aan de Kennismatrix */
		$beschikbaarheid_bestaat = select_query_array($cxn, "SELECT beschikbaarheid_id FROM tb_beschikbaarheid ".
			"WHERE uren = ".$post["number-uren-req"]." ".
			"AND dienstvorm = '".mb_convert_case($post["dienstvorm-req"], MB_CASE_LOWER, "UTF-8")."' ".
			"AND dagen = ".$post["werkdagen"]." ".
			"AND actieradius = ".$post["number-actieradius-req"], null, "beschikbaarheid_id");

		if(count($beschikbaarheid_bestaat)) {
			$beschikbaarheid_id = reset($beschikbaarheid_bestaat);
		} else {
			mysqli_query($cxn, "INSERT INTO tb_beschikbaarheid (uren, dienstvorm, dagen, actieradius)".
				" VALUES (".$post["number-uren-req"].", '".mb_convert_case($post["dienstvorm-req"], MB_CASE_LOWER, "UTF-8")."', ".$post["werkdagen"].", ".$post["number-actieradius-req"].")");
			$beschikbaarheid_id = select_query_array($cxn, "SELECT MAX(beschikbaarheid_id) FROM tb_beschikbaarheid", null, "MAX(beschikbaarheid_id)");
			$beschikbaarheid_id = reset($beschikbaarheid_id);
		}

		/* Voeg vacature toe aan de Kennismatrix */
// ".$post["taken"].",
		mysqli_query($cxn, "INSERT INTO tb_vacature (bedrijf_id, functie_id, functie_aanleiding, functie_verantwoordelijkheden, beschikbaarheid_id, aanbieding, omschrijving_overig)".
			" VALUES (".$post["number-bedrijf_id"].", ".$functie_id.", '".$files2save["aanleiding"]."', '".$files2save["verantwoordelijkheden"]."', ".$beschikbaarheid_id.", '".$files2save["aanbiedingen"]."', '".$files2save["overig"]."')");
		
	} else {
		$error["file-overig-req"] = (isset($error["file-overig-req"]) ? $error["file-overig-req"]."<br />" : "").
			"Cannot create directory ".$path;
	}
}
echo "\n\npost\n";
if(isset($post)) print_r($post);
//echo "\n\nerror\n";
//if(isset($error)) print_r($error);
$bedrijven = select_query_array($cxn, "SELECT bedrijf_id, naam FROM tb_bedrijf".($_SESSION["user"]["type"] === "Administrator" ? "" : " WHERE users_id = ".$_SESSION["user"]["users_id"]), "bedrijf_id", "naam");
$functie = select_query_array($cxn, "SELECT functie_id, functie FROM tb_functie", "functie_id", "functie");
$competenties = select_query_array($cxn, "SELECT * FROM tb_competentie_omschrijving");
$type_competenties = select_query_array($cxn, "SELECT * FROM tb_type_competentie");
$nivos = select_query_array($cxn, "SELECT * FROM tb_niveau");
$niveaus = array();
foreach($nivos as $nivo) {
	$niveaus[$nivo["niveau_id"]] = $nivo["kwalificatie"];
}
$vacatures = select_query_array($cxn, "SELECT * FROM tb_vacature WHERE bedrijf_id IN ".
	"(SELECT bedrijf_id FROM tb_bedrijf WHERE users_id = ".$_SESSION["user"]["users_id"].")");

if(count($vacatures)) {
?>
<div class="vacancy-list">
	<form id="" action="" method="">
		<h4>Vacatures</h4>
		<div class="listbox">
			<ul>
<?php
	foreach($vacatures as $vacature) {
?>
				<li <?php echo "title=\"".$functie[$vacature["functie_id"]]." bij ".$bedrijven[$vacature["bedrijf_id"]]."\""; ?>>
					<input type="checkbox" id="" name="" />
					<label onclick="javascript:document.getElementById('').checked = !document.getElementById('').checked">
						<?php echo $functie[$vacature["functie_id"]]." bij ".$bedrijven[$vacature["bedrijf_id"]]; ?>
					</label>
				</li>
<?php
	}
?>
			</ul>
		</div>
		<a class="submit button" href="#" title="Toevoegen">Toevoegen</a>
		<a class="submit button" href="#" title="Muteren">Muteren</a>
		<a class="red submit button" href="#" title="Verwijderen">Verwijderen</a>
	</form>
</div>
<?php
}
?>
<div>
	<form id="vacancy-form" action="" method="post">
		<dl>
			<dt>
				<div>
					<input type="text" name="functie-req" placeholder="Functie" title="Functie" required
					<?php if(isset($_POST["functie-req"])) echo "value=\"".$_POST["functie-req"]."\""; ?> />
				</div>
			</dt>
			<dd>
<?php
if(isset($error["functie-req"])) echo $error["functie-req"];
?>
			</dd>

			<dt>
				<div>
					<textarea name="file-aanleiding-req" placeholder="Functie ter aanleiding van" title="Functie ter aanleiding van..." required><?php
					if(isset($_POST["file-aanleiding-req"])) echo $_POST["file-aanleiding-req"];
					?></textarea>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["file-aanleiding-req"])) echo $error["file-aanleiding-req"];
?>
			</dd>

			<dt>
				<div>
<?php
if($_SESSION["user"]["type"] === "Administrator") {
?>
					<select name="number-bedrijf_id" title="Bij..." required>
						<option disabled selected hidden>- Kies organisatie -</option>
<?php
	if(count($bedrijven)) {
		foreach($bedrijven as $id => $bedrijf) {
?>
						<option <?php echo "value=\"".$id."\""; ?>><?php echo $bedrijf; ?></option>
<?php
		}
	}
?>
					</select>
<?php
} else {
	$id = key($bedrijven);
?>
					<input type="hidden" name="number-bedrijf_id" <?php echo "value=\"".$id."\""; ?> />
					<p>
<?php
	echo $bedrijven[$id];
}
?>
					</p>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["file-aanleiding-req"])) echo $error["file-aanleiding-req"];
?>
			</dd>
		</dl>
<?php
include "content/include/competentiesform.php";
include "content/include/beschikbaarheidform.php";
?>

		<dl>
			<dt>
				<div>
					<textarea name="file-verantwoordelijkheden-req" placeholder="Verantwoordelijkheden" title="Verantwoordelijkheden" required><?php
					if(isset($_POST["file-verantwoordelijkheden-req"])) echo $_POST["file-verantwoordelijkheden-req"];
					?></textarea>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["file-verantwoordelijkheden-req"])) echo $error["file-verantwoordelijkheden-req"];
?>
			</dd>

			<dt>
				<div>
					<textarea name="file-aanbiedingen-req" placeholder="Aanbiedingen" title="Aanbiedingen" required><?php
					if(isset($_POST["file-aanbiedingen-req"])) echo $_POST["file-aanbiedingen-req"];
					?></textarea>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["file-aanbiedingen-req"])) echo $error["file-aanbiedingen-req"];
?>
			</dd>

			<dt>
				<div>
					<textarea name="file-overig-req" placeholder="Omschrijving overig" title="Omschrijving overig" required><?php
					if(isset($_POST["file-overig-req"])) echo $_POST["file-overig-req"];
					?></textarea>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["file-overig-req"])) echo $error["file-overig-req"];
?>
			</dd>
		</dl>
		<a class="submit button" href="javascript:document.getElementById('vacancy-form').submit()">Toepassen</a>
	</form>
</div>
