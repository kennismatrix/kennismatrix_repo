<?php
//include "./include/is_user.php";

$users = select_query_array($cxn, "SELECT * FROM tb_users WHERE username != 'Gast'", "users_id");
$group = select_query_array($cxn, "SELECT groep_id, naam FROM tb_groep", "groep_id", "naam");

$users_names = select_query_array($cxn, "SELECT users_id, username FROM tb_users", "users_id", "username");
$user_ids = array_keys($users_names);

$users_emails = select_query_array($cxn, "SELECT users_id, emailadres FROM tb_users", "users_id", "emailadres");

$cities = select_query_array($cxn, "SELECT DISTINCT plaats FROM tb_cursist UNION (SELECT DISTINCT plaats FROM tb_bedrijf WHERE plaats NOT IN (SELECT DISTINCT plaats FROM tb_cursist))");

if($_SESSION["user"]["type"] === "Administrator") {
?>
<div class="user-list">
	<h4>Gebruikersprofielen</h4>
	<div class="listbox">
		<ul>
<!--			<li>
				<a href="?pagina=nieuw gebruiker" title="Nieuwe gebruiker toevoegen">+ Nieuwe gebruiker toevoegen</a>
			</li>
--><?php
	if(count($users)) {
		foreach($users as $id => $user) {
?>
			<li>
				<span><?php echo $group[$user["groep_id"]]."\n"; ?></span>:&ensp;
				<a <?php echo "href=\"?pagina=profiel&amp;user=".$id."\" title=\"".$user["username"]." (".$user["emailadres"].")\""; ?>>
					<span>
						<?php echo $user["username"]."\n"; ?>
					</span>
					&ensp;
					<span>
						<?php echo "(".$user["emailadres"].")\n"; ?>
					</span>
				</a>
			</li>
<?php
		}
	}
?>
		</ul>
	</div>
</div>
<?php
}

if(isset($_SESSION["user"])) {
	$user = (isset($_GET["user"]) && $_SESSION["user"]["type"] === "Administrator" ? $_GET["user"] : $_SESSION["user"]["users_id"]);
}

if(isset($user)) {

?>
<p>
	<small>Profiel: <?php echo $users_names[$user]." (".$users_emails[$user].")"; ?></small>
</p>
<div class="profile-form">
	<form id="profile-form" action="" method="post">
		<dl>
			<dt>
				<div>
					<input type="text" list="plaats" name="plaats-req" placeholder="Plaats" title="Plaats" required
					<?php if(isset($_POST["plaats-req"])) echo "value=\"".$_POST["plaats-req"]."\""; ?> />
					<datalist id="plaats">
<?php
	foreach($cities as $city) {
?>
						<option <?php echo "value=\"".$city["plaats"]."\""; ?>></option>
<?php
	}
?>
					</datalist>
				</div>
			</dt>
			<dd>
<?php
	if(isset($error["plaats-req"])) echo $error["plaats-req"];
?>
			</dd>
		</dl>

<?php
	if(/* admin */ $group[$users[$user]["groep_id"]] === "Administrator") {
		include "content/".$pages["rechten"];
	} elseif(/* bedrijf */ $group[$users[$user]["groep_id"]] === "Bedrijf") {
		include "content/".$pages["vacatures"];
	} else /* cursist */ {
		include "content/include/competentiesform.php";
		include "content/include/beschikbaarheidform.php";
	}
?>
		<a class="submit button" href="javascript:document.getElementById('profile-form').submit()" title="Toepassen">Toepassen</a>
	</form>
</div>
<?php
} elseif(isset($_SESSION["user"]) && $_SESSION["user"]["type"] === "Administrator" && isset($_GET["user"])) {
?>
<p>
	<small>Ongeldige gebruiker</small>
</p>
<?php
}
?>
