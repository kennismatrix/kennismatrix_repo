<?php
if(isset($_GET["bedrijf"]) && in_array($_GET["bedrijf"], array_keys($bedrijven_id))) {
	$bedrijf_id = $_GET["bedrijf"];
} elseif(isset($_GET["bedrijf"])) {
	$error["bedrijf"] = "Ongeldig bedrijf";
}

$bedrijven = select_query_array($cxn, "SELECT * FROM tb_bedrijf", "bedrijf_id");

$vacatures = select_query_array($cxn, "SELECT * FROM tb_vacature".
	(isset($bedrijf_id) ? " WHERE bedrijf_id = ".$bedrijf_id : "").
	" ORDER BY plaatsingsdatum DESC"
);

$vaardigheden = select_query_array($cxn, "SELECT vaardigheid_id, naam FROM tb_vaardigheid", "vaardigheid_id", "naam");

$functie = select_query_array($cxn, "SELECT functie_id, functie FROM tb_functie", "functie_id", "functie");
?>
<div>
<?php
if(count($vacatures)) {
	foreach($vacatures as $vacature) {
		$datum = $vacature["plaatsingsdatum"];
		$vacature_vaardigheden = select_query_array($cxn, "SELECT vaardigheid_id, niveau FROM tb_vacature_vaardigheden WHERE vacature_id = ".$vacatures["vacature_id"]);
		$bedrijf = $vacature["bedrijf_id"];
		$contacten = select_query_array($cxn, "SELECT * FROM tb_contactpersoon WHERE bedrijf_id = ".$bedrijf." ORDER BY achternaam ASC", "contactpersoon_id");
		$beschikbaarheid = select_query_array($cxn, "SELECT * FROM tb_beschikbaarheid WHERE beschikbaarheid_id = ".$vacature["beschikbaarheid_id"]);
		$dagen = werkdagen($beschikbaarheid["dagen"]);
?>
	<div class="vacature">
		<div><?php echo $werkdagen[date("w", $datum)].", ".date("j", $datum).$maand[date("n", $datum)-1]." ".date("Y", $datum); ?></div>
		<div><?php echo (time()-$datum)/86400; ?> dagen geleden</div>

		<div class="summary">
<?php
		foreach($vacature_vaardigheden as $vaardigheid) {
?>
			<div><?php echo $vaardigheden[$vaardigheid["vaardigheid_id"]]." ".$vaardigheid["niveau"]; ?> %</div>
<?php
		}
?>
		</div>

		<div class="details">
<?php
		if(strpos($onbetaald, $_SESSION["user"]["code"][0]) !== false) {
?>
			<div>
				<span><?php echo $bedrijven[$bedrijf]["naam"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["straat"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["huisnummer"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["postcode"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["plaats"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["land"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["emailadres"]; ?></span>
				<span><?php echo $bedrijven[$bedrijf]["telefoon"]; ?></span>
			</div>
<?php
			if(count($contacten)) {
				foreach($contacten as $contact) {
?>
			<div>
				Neem contact op met:
				<span><?php echo $contact["achternaam"]; ?></span>
				<span><?php echo $contact["tussenvoegsel"]; ?></span>
				<span><?php echo $contact["voorletters"]; ?></span>
				<span><?php echo $contact["voornaam"]; ?></span>
				<span><?php echo $contact["functie"]; ?></span>
				<span><?php echo $contact["telefoon"]; ?></span>
				<span><?php echo $contact["emailadres"]; ?></span>
			</div>
<?php
				}
			}
		}
?>
			<div>
				<h2><?php echo $functie[$vacature["functie_id"]]; ?></h2>
				<div>
<?php
		$str = file_get_contents("vacature/".$vacature["vacature_id"]."/".$vacature["omschrijving_overig"]);
		if($str !== false) echo $str;
 ?>
				</div>
				<div>
<?php
		$str = file_get_contents("vacature/".$vacature["vacature_id"]."/".$vacature["functie_aanleiding"]);
		if($str !== false) echo $str;
?>
				</div>
				<div>
<?php
		$str = file_get_contents("vacature/".$vacature["vacature_id"]."/".$avacture["functie_verantwoordelijkheden"]);
		if($str !== false) echo $str;
?>
				</div>
			</div>

			<div>
				<div>
					<span><?php echo $beschikbaarheid["uren"]; ?> uur</span>
					<span><?php echo $beschikbaarheid["dienstvorm"]; ?></span>
				</div>

				<div>Werkdagen:<div>
				<ul>
<?php
		if(count($dagen)) {
			foreach($dagen as $dag) {
?>
					<li><?php echo $werkdagen[$dag]; ?></li>
<?php
			}
		}
?>
				</ul>

				<div>
					Woonachtig binnen een straal van <?php echo $beschikbaarheid["actieradius"]; ?> kilometer van <?php echo $bedrijven[$bedrijf]["plaats"]; ?>
				</div>
			</div>

			<div>
				<div>
<?php
		$str = file_get_contents("vacature/".$vacature["vacature_id"]."/".$vacature["aanbieding"]);
		if($str !== false) echo $str;
?>
				</div>
			</div>
		</div>
	</div>
<?php
	}
} else {
?>
	<div>
		Momenteel heeft het systeem geen vacatures :(
	</div>
<?php
	if($_SESSION["user"]["type"] !== "Student" || $_SESSION["user"]["type"] !== "Gast") {
?>
	<div>
		Voeg <a href="?pagina=vacature toevoegen">hier</a> een vacature toe.
	</div>
<?php
	}
}
?>
</div>
