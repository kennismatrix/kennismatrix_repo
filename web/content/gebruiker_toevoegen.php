<?php
$groups = select_query_array($cxn, "SELECT * FROM tb_groep");
?>
<div class="gebruiker-form">
	<form id="gebruikers-form" action="" method="post">
		<dl>
			<dt>
				<div>
					<input type="text" name="number-zoho_id" placeholder="Zoho ID" title="Zoho ID"
					<?php if(isset($_POST["number-zoho_id"])) echo "value=\"".$_POST["number-zoho_id"]."\""; ?> />
				</div>
			</dt>
			<dd>
<?php
if(isset($error["number-zoho_id"])) echo $error["number-zoho_id"];
?>
			</dd>

			<dt>
				<div>
					<select name="groep-req" title="Gebruikersgroep" required>
						<option disabled selected hidden>- Kies een gebruikersgroep -</option>
<?php
if(count($groups)) {
	foreach($groups as $group) {
?>
						<option <?php
		echo "value=\"".$group["groep_id"]."\""; 
		if(isset($_POST["groep-req"]) && $_POST["groep-req"] === $group["groep_id"]) echo " selected"; 
						?>><?php echo $group["naam"]; ?></option>
<?php
	}
}
?>
					</select>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["groep-req"])) echo $error["groep-req"];
?>
			</dd>

			<dt>
				<div>
					<input type="text" name="email-adres-req" placeholder="E-mailadres" title="E-mailadres" required
					<?php if(isset($_POST["email-adres-req"])) echo "value=\"".$_POST["email-adres-req"]."\""; ?> />
				</div>
			</dt>
			<dd>
<?php
if(isset($error["email-adres-req"])) echo $error["email-adres-req"];
?>
			</dd>

			<dt>
				<div>
					<input type="checkbox" name="activeren" title="Gebruiker activeren"
<?php
	if(isset($_POST["activeren"])) {
		if($_POST["activeren"] == 1) {
			echo "checked";
		}
	} else {
		echo "checked";
	}
?> />
					<label for="activeren">Gebruiker activeren</label>
				</div>
			</dt>
			<dd>
<?php
if(isset($error["activeren"])) echo $error["activeren"];
?>
			</dd>
		</dl>
		<a class="submit button" href="javascript:document.getElementById('gebruikers-form').submit()" title="Aanmaken">Aanmaken</a>
	</form>
</div>
