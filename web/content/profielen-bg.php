<?php
$gebruiker = select_query_array($cxn, "SELECT users_id, username FROM tb_users", "users_id", "username");
$kandidaten = select_query_array($cxn, "SELECT cursist_id, users_id FROM tb_cursist", "users_id", "cursist_id");
?>
<div>
<?php
if(count($kandidaten)) {
	$i = 1;
	foreach($kandidaten as $id => $kandidaat) {
		$vaardigheden = select_query_array($cxn, "SELECT naam, niveau FROM tb_cursist_vaardigheid, tb_vaardigheid WHERE tb_cursist_vaardigheid.vaardigheid_id = tb_vaardigheid.vaardigheid_id AND cursist_id = ".$kandidaat, "naam", "niveau");
?>
	<div>
		<h4>
<?php
	echo (strpos($onbetaald, $_SESSION["user"]["code"][0]) === false ? 
		$gebruiker[$id]." ".
			($_SESSION["user"]["type"] === "Administrator" ? " (Profiel kandidaat ".$i.")" : "") : 
		"Profiel kandidaat ".$i);
?>
		</h4>
		<ol>
<?php
		foreach($vaardigheden as $vaardigheid => $niveau) {
?>
			<li>
				<span><?php echo $vaardigheid; ?></span>
				&ndash;
				<span><?php echo $niveau; ?>%</span>
			</li>
<?php
		}
?>
		</ol>
	</div>
<?php
		$i++;
	}
}
?>
</div>
