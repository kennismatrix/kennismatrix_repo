<?php
$bedrijven = select_query_array($cxn, "SELECT bedrijf_id, naam, straat, huisnummer, postcode, plaats, land, emailadres, telefoon FROM tb_bedrijf ORDER BY naam ASC");

$nBedrijven = count($bedrijven);
for($i = 0; $i < $nBedrijven; $i++) {
	$bedrijven[$i]["contacten"] = select_query_array($cxn, "SELECT voornaam, voorletters, tussenvoegsel, achternaam, functie, emailadres, telefoon FROM tb_contactpersoon WHERE bedrijf_id = ".$bedrijven[$i]["bedrijf_id"]." ORDER BY achternaam ASC");
}

?>
<div>
<?php
foreach($bedrijven as $bedrijf) {
?>
	<hr />
	<div class="row">
		<div>
			<div class="bedrijf">
				<strong><?php echo $bedrijf["naam"]; ?></strong>,
				<span><?php echo $bedrijf["straat"]; ?></span>
				<span><?php echo $bedrijf["huisnummer"]; ?></span>,
				<span><?php echo substr_replace($bedrijf["postcode"], " ", 4, 0); ?></span>,
				<span><?php echo $bedrijf["plaats"]; ?></span>,
				<span><?php echo $bedrijf["land"]; ?></span>,
				<span><a <?php echo "href=\"mailto:".$bedrijf["emailadres"]."\""; ?>><?php echo $bedrijf["emailadres"]; ?></a></span>,
				<span><?php echo $bedrijf["telefoon"]; ?></span>
			</div>
			<ul>
<?php
	foreach($bedrijf["contacten"] as $contact) {
?>
				<li>
					<span><?php echo $contact["achternaam"]; ?></span>
					<span><?php echo $contact["tussenvoegsel"]; ?></span>,
					<span><?php echo $contact["voorletters"]; ?></span>
					<span><?php if(!empty($contact["voornaam"])) echo "(".$contact["voornaam"].")"; ?></span>,
					<span><?php echo $contact["functie"]; ?></span>
					<span><a <?php echo "href=\"mailto:".$contact["emailadres"]."\""; ?>><?php echo $bedrijf["emailadres"]; ?></a></span>
					<span><?php echo $contact["telefoon"]; ?></span>
				</li>
<?php
	}
?>
			</ul>
		</div>
	</div>
<?php
}
?>
</div>
