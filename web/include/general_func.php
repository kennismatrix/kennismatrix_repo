<?php
//define("", "//");
/**/
/* Dutch, Belgium or German postal code */
define("REGEX_POSTAL_CODE", "/^[0-9]{4}((\s?[A-Za-z]{2})|[0-9])?$/");
define("REGEX_EMAIL", "/[a-z0-9!#$%\&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%\&'*+\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-zA-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)\b/");
define("REGEX_NAME", "/\s?([A-Z]?[a-z\-]+|([A-Z]\.)+)/");
define("REGEX_TEL_NUMBER", "/^(\+|00?)?[1-9\-\s\.][0-9\-\s\.]{10,25}/");
define("REGEX_ALPHANUM", "/[A-Za-z0-9\s]/");
define("REGEX_HOME_NUMBER", "/^[A-Za-z]?[1-9][0-9]{0,6}[\-\s\.\^\*\']?([A-Za-z]|alfa|bis|gamma)?([1-9][0-9]{3})?/");
define("REGEX_PATH", "/(([A-Za-z][\w+\.\-\_\s]+)(:\/\/|(\/|\.\w{1,4}))+)/");
define("REGEX_PWD_ADMIN", "/(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*/");
define("REGEX_PWD_USER", "/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/");
/*
Note:
  These regexes for dates does not check whether a day of the week corresponds with the right date. This should be solved with JS or PHP.
  However it does check leap years and limits the number of days of a month.
  The regex limits till 2099, otherwise '99.
*/
/* Dutch days of the week */
define("REGEX_DAY", "/^(((zon?|maa?|din?|woe?|don?|vr(ij)?|zat?)|(zon|maan|dins|woens|donder|vrij|zater)dag),? )?/");
/* Dutch months */
define("REGEX_MONTH", "/(jan(uari)?|feb(ruari)?|m(aa(rt)?|rt)|apr(il)?|mei|ju(n|l)i?|aug(ustus)?|sept?(ember)?|okt(ober)?|(nov|dec)(ember)?)/");
define("REGEX_DATE", "/(".
/* Dutch 28-day month "february" */
"((0?[1-9]|[12][0-8])[\ \.\-\/](0?2|feb(ruari)?)[\ \.\-\/](20|\')?[0-9]{2})|".
/* Dutch 29-day month "leap years" */
"(29[\ \.\-\/](0?2|feb(ruari)?)[\ \.\-\/](20|\')?([02468][048]|[13579][26]))|".
/* Dutch 30-day months */
"((0?[1-9]|[12][0-9]|30)[\ \.\-\/]((0?[13-9]|1[012])|jan(uari)?|m(aa(rt)?|rt)|apr(il)?|mei|ju(n|l)i?|aug(ustus)?|sept?(ember)?|okt(ober)?|(nov|dec)(ember)?)[\ \.\-\/](20|\')?[0-9]{2})|".
/* Dutch 31-day months */
"(31[\ \.\-\/]((0?[13578]|1[02])|jan(uari)?|m(aa(rt)?|rt)|mei|juli?|aug(ustus)?|okt(ober)?|dec(ember)?)[\ \.\-\/](20|\')?[0-9]{2})".
")$/");


$regex_vars = array(
	"postal_code",
	"email",
	"name",
	"tel_number",
	"alphanum",
	"home_number",
	"path",
	"day",
	"month",
	"date",
	"password_user",
	"password_admin",
	"number",
	"percentage"
);

$werkdagen = array(
	"zondag",
	"maandag",
	"dinsdag",
	"woensdag",
	"donderdag",
	"vrijdag",
	"zaterdag"
);

$content_file_types_post = array(
	"mail" => "mail",
	"nieuws" => "news",
	"vacature toevoegen" => "vacancies"
);




function werkdagen($code)
{
	if(!is_numeric($code)) return null;

	$code = intval($code);

	if($code < 1) return null;
	if($code >= 128) return array(0, 1, 2, 3, 4, 5, 6);

	$dagen = array();
	$i = 6;
	while($code > 0 && $i >= 0) {
		if($code >= pow(2, $i)) {
			$code -= pow(2, $i);
			$dagen[] = $i;
		}

		$i--;
	}

	return $dagen;
}

$maand = array(
	"januari",
	"februari",
	"maart",
	"april",
	"mei",
	"juni",
	"juli",
	"augustus",
	"september",
	"oktober",
	"november",
	"december"
);


function select_query_array($cxn, $query, $key_column = null, $value_column = null, &$result = null)
{
	if(empty($cxn) || !is_string($query) || strlen($query) == 0) {
		return null;
	}

	$query_result = mysqli_query($cxn, $query) or die("'".$query."' is niet uitvoerbaar");

	$nrows = mysqli_num_rows($query_result);

	if($nrows > 0) {
		for($i=0; $i<$nrows; $i++) {
			$result[] = mysqli_fetch_assoc($query_result);
		}
	}

	$list = array();
	if(isset($value_column) && count($result)) {
		foreach($result as $res) {
			if(isset($key_column)) {
				$list[$res[$key_column]] = $res[$value_column];
			} else {
				$list[] = $res[$value_column];
			}
		}
	} elseif(isset($key_column) && count($result)) {
		foreach($result as $res) {
			$list[$res[$key_column]] = $res;
			unset($list[$res[$key_column]][$key_column]);
		}
	} else {
		$list = $result;
	}

	return $list;
}

function base2($exp)
{
	return pow(2, $exp);
}

function get_id($cxn, $criteria_field, $criteria_value, $table)
{
	if(empty($cxn) || !is_string($criteria_field) || !is_string($criteria_value) || !is_string($table)) {
		return null;
	}

	$id = str_ireplace("tb_", "", $table)."_id";
	$id = select_query_array($cxn, "SELECT ".$id." FROM ".$table." WHERE ".$criteria_field." = '".$criteria_value."'");

	if(empty($id) || !is_array($id) || !isset($id[0])) {
		return null;
	}

	return reset($id[0]);
}


function postal_code($code)
{
	if(!is_string($code)) {
		return null;
	}

	// Belgium or German postal code
	if(preg_match("/^[0-9]+$/", $code)) return $code;

	// Dutch postal code
	if(preg_match("/^[1-9][0-9]{3}\s?[A-Za-z]{2}$/", $code)) {
		$code = strtoupper(str_ireplace(" ", "", $code));
	}

	return $code;
}


function user_exists($cxn, $username)
{
	if(empty($cxn) || !is_string($username)) {
		return null;
	}

	$users = select_query_array($cxn, "SELECT username FROM tb_users", null, "username");
//	print_r($users);

	return in_array($username, $users);
}


function strinc(&$strval, $inc = 1)
{
	if(!is_numeric($strval) || !is_numeric($inc)) return null;

	$inc = floatval($inc);
	$strval = floatval($strval);

	$fstrval = $strval + $inc;

	$strval = strval($fstrval);

	return $fstrval;
}

?>
