<?php
define("DIR_ONLY",1);
define("FILES_ONLY",2);
define("PROJ_LEN",15);

$data_labels = array(
	"date of release",
	"development hours"
);

function strchop($str, $chopper = ".")
{
	return (strpos($str, $chopper) > 0 ? rtrim(rtrim($str,"a..zA..Z0..9"), $chopper) : $str);
//	return implode($chopper, explode($chopper, $str));
}


function dir_list($list)
{
	return !preg_match("/^(\.|\.\.)$/",$list);
}


function reset_keys($a)
{
	$result = array();
	foreach($a as $key => $value) {
		$result[] = $value;
	}

	return $result;
}


function map_files_filter($dir, $filter = 0, $include = array(), $exclude = array(), $show_hidden = false)
{
	$list = array();
	$filtered_list = array();

	if(file_exists($dir) && is_dir($dir)) {
		$list = array_values(array_diff(scandir($dir), array("..","."), $exclude));

		if($filter > 0 && $n = count($list)) {
			foreach($list as $file) {
				if(isset($file) && strlen($file)) {
					if(
						$filter === DIR_ONLY && is_dir($dir."/".$file) ||
						$filter === FILES_ONLY && is_file($dir."/".$file) ||
						!empty($include) && in_array($file, $include)
					) {
						if($file[0] === ".") {
							if($show_hidden) {
								$filtered_list[] = $file;
							}
						} else {
							$filtered_list[] = $file;
						}
					}
				}
			}
		} else {
			$filtered_list = $list;
		}
	}

	return $filtered_list;
}
/*
function alt_file_seek($path)
{
	if(!is_string($path)) return null;

	$directories = explode("/", $path);

	$file_to_search_for = strchop(strchop(end($directories)), "-");

	unset($directories[count($directories-1)]);
	$directories = implode("/", $directories);

	$files = map_filter_files($directories, FILES_ONLY);

	if(count($files)) {
		foreach($files as $file) {
			$file_list[] = strchop($file);
		}

		// seek a file that has $file_to_search_for in $file_list
		$i = 0;
		while(!isset($found_file) && $i < strlen($file_to_search_for)) {
			foreach($file_list as $file) {
				if($file[$i] === $file_to_search_for[$i]) {
					$files[] = $file;
				}
			}

			if(count($files) > 1 && $i < strlen($file_to_search_for)) {
				$file_list = $files;

				$i++;
			} else {
				if(count($files)) {
					$file_list = $files;
				}

				$found_file = $file_list[0];
			}

			print_f($file_list);
			echo "\n\n";
		}

		return $directories."/".$found_file;
	}

	return null;
}
*/
?>
