<?php
/* ini_set */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', TRUE);
ini_set('output_buffering', 4096);

/* globals */

/* includes, classes and functions */
include "include/map_files_filter.php";
include "include/general_func.php";




$base = "kennisbank.nl.eu.org";

/* DATABASE CONNECTION */

$address = ($_SERVER["SERVER_NAME"] !== $base ? "localhost" : $base);
$user = "kennismatrix";
$password = "6uqes3%!@rFC-Uu-";
$database = "admin_kennisbank";
$cxn = mysqli_connect($address, $user, $password, $database) or
	die("De Kennismatrix kan geen verbinding maken met de MySQL-databank.");





/* SESSION COOKIE */

session_start();

$_SESSION["protocol"] = (empty($_SERVER["HTTPS"]) ? "http" : "https");
$_SESSION["using_ssl"] = ($_SESSION["protocol"] === "https");
$_SESSION["domain"] = $_SERVER["SERVER_NAME"];
$_SESSION["web_path"] = implode("/", explode("/", $_SERVER["REQUEST_URI"], -1))."/";
$_SESSION["url"] = $_SESSION["protocol"]."://".$_SESSION["domain"].$_SESSION["web_path"];
$_SESSION["path"] = $_SERVER["DOCUMENT_ROOT"]; // unused
/*
$_SESSION["cookie"] = array();
$_SESSION["cookie"]["name"] = "kennismatrix";
$_SESSION["cookie"]["httponly"] = true;
$_SESSION["session"] = array();
$_SESSION["session"]["lifetime"] = 60; // in minutes
*/

if(isset($_SESSION["user"])) {
	$_SESSION["files"] = map_files_filter("content", FILES_ONLY);
	$pages = array();
	foreach($_SESSION["files"] as $filename) {
		$label = str_ireplace(array(" ", "_"), array("", " "), strchop(strchop($filename), "-"));
		$seperate_rights = (strpos($filename, "-") !== false ? explode("-", strchop($filename)) : "");
		$_SESSION["file_rights"][$label] = ($seperate_rights === "" ? "" : end($seperate_rights));
		$pages[$label] = $filename;
	}

	$labels = array_keys($pages);
	array_push($labels, "logout");

	$_SESSION["file_rights"]["logout"] = "A";

	$type = select_query_array($cxn, "SELECT naam FROM tb_type_groep WHERE type_groep_id = (SELECT type FROM tb_groep WHERE groep_id = ".$_SESSION["user"]["groep_id"].")", null, "naam");
	$_SESSION["user"]["type"] = $type[0];
	$code = ($_SESSION["user"]["betaald"] ? strtoupper($type[0][0]) : strtolower($type[0][0]));

	$_SESSION["user"]["code"] = $code.array_search($code, select_query_array($cxn, "SELECT naam FROM tb_type_groep WHERE naam LIKE '".$code[0]."%'", null, "naam"));

	$menu_items = array();
	foreach($labels as $label) {
		if($_SESSION["user"]["type"] === "Administrator" || 
			$_SESSION["file_rights"][$label] === "A" ||
			stripos($_SESSION["file_rights"][$label], $_SESSION["user"]["code"][0]) !== false) {

			$menu_items[] = $label;
		}
	}
}

$onbetaald = "";
$usercodes = select_query_array($cxn, "SELECT naam FROM tb_type_groep", null, "naam");
foreach($usercodes as $usercode) {
	$onbetaald .= $usercode[0];
}

$onbetaald = strtolower($onbetaald);





/* GET */

$page = "";
if($_GET) {
	if(isset($_GET["pagina"])) {
		if((isset($_SESSION["user"]) && in_array($_GET["pagina"], $menu_items, true)) || 
			(!isset($_SESSION["user"]) && $_GET["pagina"] === "login")) {
			$page = $_GET["pagina"];

			if($page === "logout") {
				unset($_SESSION["user"]);
				header("Location: index.php?pagina=login");
				exit;
			}

			if($_POST) {
				print_r($_POST);
				$save2file = array();
				foreach($_POST as $key => $value) {
					$veld = explode("-", str_ireplace("-req", "", $key));
					$veld = end($veld);
					if(!is_array($value)) {
						if(preg_match("/^file-/", $key)) {
							$save2file[$veld] = trim(htmlspecialchars($value));
						} else {
							$post[$key] = trim(htmlspecialchars(strip_tags($value)));

							foreach($regex_vars as $var) {
								if(preg_match("/".$var."-req$/", $key) && empty($post[$key])) {
									$error[$key] = mb_convert_case($veld, MB_CASE_TITLE, "UTF-8")." is een verplicht veld";
								} elseif(preg_match("/^".$var."-/", $key)) {
									if(
										$var === $regex_vars[0] && !preg_match(REGEX_POSTAL_CODE, $post[$key]) ||
										$var === $regex_vars[1] && !preg_match(REGEX_EMAIL, $post[$key]) ||
										$var === $regex_vars[2] && !preg_match(REGEX_NAME, $post[$key]) ||
										$var === $regex_vars[3] && !preg_match(REGEX_TEL_NUMBER, $post[$key]) ||
										$var === $regex_vars[4] && !preg_match(REGEX_ALPHANUM, $post[$key]) ||
										$var === $regex_vars[5] && !preg_match(REGEX_HOME_NUMBER, $post[$key]) ||
										$var === $regex_vars[6] && !preg_match(REGEX_PATH, $post[$key]) ||
										$var === $regex_vars[7] && !preg_match(REGEX_DAY, $post[$key]) ||
										$var === $regex_vars[8] && !preg_match(REGEX_MONTH, $post[$key]) ||
										$var === $regex_vars[9] && !preg_match(REGEX_DATE, $post[$key]) ||
										$var === $regex_vars[10] && !preg_match(REGEX_PWD_ADMIN, $post[$key]) ||
										$var === $regex_vars[11] && !preg_match(REGEX_PWD_USER, $post[$key]) ||
										$var === $regex_vars[12] && !is_numeric($post[$key]) ||
										$var === $regex_vars[13] && !is_numeric($post[$key]) && 
											floatval($post[$key]) < 0 && floatval($post[$key]) > 100
									) {
										$error[$key] = "Ongeldige invoer - ".is_numeric($post[$key]);
									}
									end($regex_vars);
								}
							}
						}
					}  else {
						$values = array_keys($value);

						$days_of_week = $werkdagen;
						$days_of_week[] = array_shift($days_of_week);

						$dagen_beschikbaar = array();
						foreach($values as $werkdag) {
							if(in_array($werkdag, $days_of_week, true)) {
								$dagen_beschikbaar[array_search($werkdag, $days_of_week, true)] = $werkdag;
							}
						}

						$post[$key] = array_sum(array_map('base2', (count($dagen_beschikbaar) ? array_keys($dagen_beschikbaar) : $values)));
					}
				}
			}
		}

		if(isset($_GET["user"])) {
			if(!isset($_SESSION["user"]) && !$_SESSION["user"]["type"] === "Administrator") {
				header("Location: index.php?pagina=".$page);
				exit;
			}
		}
	}
}



if(isset($_SESSION["user"]) && (empty($page) || $page === "login")) {
	$page = reset($menu_items);

	header("Location: index.php?pagina=".$page);
	exit;
} elseif(!isset($_SESSION["user"]) && (!isset($_GET["pagina"]) || $page !== "login")) {
	header("Location: index.php?pagina=login");
	exit;
}



/* SESSION COOKIE */

$_SESSION["title"] = "KennisMatrix :: ".mb_convert_case($page, MB_CASE_TITLE, "UTF-8");

?>

<!DOCTYPE html>
<html lang="nl">
<head>
<!-- meta standard -->
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<!-- meta author -->
<meta name="author" content="analy3.org - M P J van Dartel" />
<!-- meta description of this page -->
<meta name="description" content="" />
<!-- meta web searches -->
<meta name="keywords" content="" />

<!-- title of the page -->
<title><?php echo $_SESSION["title"]; ?></title>

</head>

<body>
<?php
if(isset($_SESSION["user"])) {
//	print_r($_SESSION["user"]);
?>
<div class="user">
	<p><small>Gebruiker: <?php echo $_SESSION["user"]["emailadres"]; ?></small></p>
</div>

<div class="main menu">
	<h3>Hoofdmenu</h3>
	<ul>
<?php
//	print_r($menu_items);
	foreach($menu_items as $menu_item) {
?>
		<li>
<?php
		if($page === $menu_item) {
			echo "\t\t\t".$menu_item."\n";
		} else {
?>
			<a <?php echo "href=\"?pagina=".$menu_item."\""; ?>><?php echo $menu_item; ?></a>
<?php
		}
?>
		</li>
<?php
	}
?>
	</ul>
</div>

<div class="content">
<?php
	if(isset($pages[$page]) && file_exists("content/".$pages[$page])) {
?>
	<h1><?php echo mb_convert_case($page, MB_CASE_TITLE, "UTF-8"); ?></h1>
<?php
		include "content/".$pages[$page];

	} elseif(file_exists("content/include/error/404.php")) {
		include "content/include/error/404.php";

	} else {
?>
	<h1>Error 404</h1>
<?php
	}
?>
</div>
<?php
} elseif(!isset($_SESSION["user"]) && $page === "login") {
	if(file_exists("content/include/loginform.php")) {
		include "content/include/loginform.php";
	} else {
?>
	<p>
		Fatale fout: Bestandsstructuur defect
	</p>
<?php
	}
}

mysqli_close($cxn);
?>
</body>

</html>
